module.exports = {
    title: 'Code B1',
    description: 'Support en ligne pour le workshop B1',
    head: [
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js' }],
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i&display=swap' }],
  ],
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { text: 'Processing', link: 'https://processing.org' },
            { text: 'P5js', link: 'https://p5js.org' }
        ],
        sidebarDepth: 0,
        sidebar: [
            {
              title: 'Généralités',
              collapsable: true,
              children: [
                '/',
                'generalites/filiation',
                'generalites/inspirations',
                //'generalites/etudiants',
                'generalites/scratch',
                'generalites/processing-et-p5js',
                'generalites/monde-physique',
                'generalites/adresses'
              ]
            },
            {
              title: 'Processing',
              collapsable: true,
              children: [
                'processing/structure',
                'processing/coordonnees',
                'processing/formes-simples',
                'processing/couleur',
                'processing/transformations',
                'processing/variables',
                'processing/conditions',
                'processing/boucles',
                'processing/fonctions',
                'processing/aleatoire',
              ]
            },
            {
              title: 'En pratique',
              collapsable: true,
              children: [
                'en-pratique/commencer',
                'en-pratique/animation',
                'en-pratique/images',
                'en-pratique/charger-svg',
                'en-pratique/interactivite-souris',
                'en-pratique/interactivite-clavier',
                'en-pratique/texte',
                'en-pratique/film',
                'en-pratique/exporter'
              ]
            },
            {
              title: 'Exercices',
              collapsable: true,
              children: [
                'exercices/formes-simples',
                'exercices/generatif',
                //'exercices/animation',
                'exercices/outil-de-dessin',
                'exercices/trames',
                'exercices/horloge',
                'exercices/slitscan'
              ]
            },
            {
              title: 'Pour clôturer',
              collapsable: true,
              children: [
                'cloture/rendus',
                'cloture/formulaire'
              ]
            }/*,
            {
              title: 'Design',
              children: [
                'design/ressources'
              ]
            },
            {
              title: 'HTML & CSS',
              children: [
                'web/ressources'
              ]
            },
            {
              title: 'Data',
              children: [
                'data/ressources'
              ]
            },
            {
              title: 'WebGL',
              children: [
                'data/ressources'
              ]
            },
            {
              title: 'Raspberry PI',
              children: [
                'raspberrypi/ressources'
              ]
            },
            {
              title: 'NodeJS et Express',
              children: [
                'node/ressources'
              ]
            },
            {
              title: 'Satellites',
              children: [
                'satellites/ressources'
              ]
            }*/
            
          ]
    }
}