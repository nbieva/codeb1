---
title: Feedback
lang: fr-FR
---

# Feedback

Le formulaire ci-dessous est anonyme. Il nous permet néanmoins d'identifier les éventuels problèmes rencontrés et améliorations à apporter pour ces workshops. Les commentaires positifs sont également les bienvenus!!

<iframe style="margin-top:30px;" src="https://docs.google.com/forms/d/e/1FAIpQLScBFk_Z1yDa5Vd-8_4YAzyY6zr3lOHuOYgCd5Zxl_HdRhOyNg/viewform?embedded=true" width="740" height="2484" frameborder="0" marginheight="0" marginwidth="0">Chargement…</iframe>
