---
title: Remise de vos travaux
lang: fr-FR
---

# Remise de vos travaux

<p class="lead">En règle générale, essayez de nous rendre tout ce que vous réalisez durant cette journée, ou du moins tout ce que vous pensez être significatif. Rassemblez le tout dans un dossier.</p>

<div class="notification danger"> <p>En fin de journée, ne partez surtout pas sans vous être assuré que vos fichiers sont bien en notre possession et que vous avez rempli le <a href="https://codeb1.netlify.com/cloture/formulaire.html">formulaire de feedback</a>.</p> </div>

Créez un dossier dans votre dossier "Documents" et nommez-le comme suit: **nom-prenom-option**
n'utilisez pas d'espace, d'accents ou de caractères spéciaux dans vos noms de fichiers ou de dossiers.
En cours de journée, lorsqu'un travail est terminé ou qu'une étape importante est franchie, déplacez vos fichiers vers ce répertoire.

En fin de journée, connectez-vous au réseau (voir ci-dessous), et déplacez votre dossier portant votre nom vers le répertoire commun (voir ci-dessous) demandez de l'aide si nécessaire.

### Pour se connecter au réseau local:

Dans le finder > **Aller > Se connecter au serveur > Parcourir** > cherchez le poste **HD45** (demandez si la référence est toujours à jour) et déposez votre dossier dans le répertoire WORKSHOP-PROCESSING (ou autre qui y ressemble..), qui devrait être présent dans le répertoire Documents.

Résumé: HD45 > étudiant > Documents > WORKSHOP-PROCESSING