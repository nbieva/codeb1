---
title: Charger un fichier SVG
lang: fr-FR
---

# Charger et utiliser un SVG

Un fichier SVG (tracé vectoriel), se charge dans Processing avec la variable PShape (comme les images avec PImage).

::: warning
Notez que votre fichier SVG doit **absolument** se trouver dans un dossier data, lui-même dans le dossier de votre sketch.
:::

```processing
PShape lines;

void setup() {
  size(800, 600);
  lines = loadShape("loumi.svg");
  background(255);
  shapeMode(CENTER);
} 

void draw(){
  //Place un rectangle blanc avec opacité réduite
  fill(255,10);
  rect(0,0,width,height);
  
  //Place les formes
  shape(lines, 250, 200, 400, 400); 
  shape(lines, 200, 400);
  shape(lines, mouseX, mouseY,mouseX,mouseX);
}
```

<a data-fancybox title="Keycodes" href="/assets/loumi.png">![Cgarger un fichier SVG](/assets/loumi.png)</a>