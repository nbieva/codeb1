---
title: Commencer avec Processing
lang: fr-FR
sidebarDepth: 1
---

# Commencer avec Processing

Ouvrez Processing. Un nouveau document s'ouvrira. 

1. **Collez**-y le code ci-dessous.
2. **Sauvez** votre document
3. **Exécutez** votre code en appuyant sur play.
4. **Augmentez**-le, modifiez-le petit à petit.
5. Sauvez régulièrement votre travail (Cmd+S)

```processing
void setup(){
    size(800,600); 
    background(220);
}

void draw() {
    ellipse(mouseX,mouseY,60,60);
}
```

### Ce même code, commenté

```processing

// Le code à l'intérieur du bloc setup ne s'excutera qu'une seule fois.
void setup(){
    //Définir la taille de votre sketch (Un peu comme choisir un format pour une feuille de papier)
    size(800,600); 

    //Définir une couleur de fond (pas indispensable mais on le fait souvent)
    background(220,220,220); //peut s'écrire background(220);
}

// Le code à l'intérieur du bloc draw s'excutera en boucle, jusqu'à ce qu'on lui dise de s'arrêter.
void draw() {
    //Inscrire ici (dans le draw) le code qui va tourner en boucle (variables qui évoluent, conditions, etc.)
    ellipse(mouseX,mouseY,60,60);
}
```