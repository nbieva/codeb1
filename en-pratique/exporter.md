---
title: Exporter
lang: fr-FR
sidebarDepth: 0
---

# Exporter

## Export PNG, JPG (bitmap)

Il existe différentes façon d'exporter une image. La fonction "save" est la plus simple à mettre en oeuvre. Veillez cependant à la placer au bon endroit dans votre sketch. Ce que vous dessinez après la commande ne sera pas exporté.

```processing
save("diagonal.jpg");
```
Vous pouvez également conditionner l'export d'une image, à une pression sur la barre d'espace par exemple. Ceci peut vous permettre de ne pas multiplier les fichiers inutiles.

```processing
if (keyPressed) {
    save("ellipse-"+frameCount+".png");
}
```

[https://processing.org/reference/keyPressed.html](https://processing.org/reference/keyPressed.html)

Attention: cette dernière façon de faire ne fonctionnera que si la condition peut être vérifiée et qu'une boucle est donc active..

```processing
void setup() {
    size(600,400);
}

void draw() {
    background(255);
    rect(50,50,mouseX,mouseY);
}

void keyPressed() {
    save("nomPrenom-option.png");
    exit();
}
```

Dans cet exemple également, chaque fichier enregistré portera le nom "diagonal.png" et écrasera le précédent sans aucune forme d'avertissement.

Afin de pouvoir enregistrer votre fichier sous un nom différent chaque fois que vous pressez une touche \(ou à chaque boucle, ou chaque fois que vous le désirez..\), il vous faudra y intégrer l'une ou l'autre variable. La solution suivante pourrait être une solution:

```processing
void setup() {
    size(600,400);
}
void draw() {
    background(255);
    rect(50,50,mouseX,mouseY);
}
void keyPressed() {
    save("nomPrenom-"+hour()+"-"+minute()+"-"+second()+".png");
    exit();
}
```

Ce qui devrait donner **doeJohn-10-22-56.png** et a toutes les chances d'être différent de vos autres fichiers.

* [Output vers un fichier txt](https://processing.org/reference/createWriter_.html)

Ex. Texte

## Export au format PDF (vectoriel)

::: tip
L'export en PDF nécessite l'importation d'une bibliothèque (fonctions supplémentaires) dans votre sketch. Une *bibliothèque* est un ensemble de fonctions utilitaires, regroupées et mises à disposition afin de pouvoir être utilisées sans avoir à les réécrire.
:::
+ Ouvrez votre sketch
+ Cliquez sur **Sketch** > **Importer une librairie** > **Ajouter une librairie** (Si vous ne voyez pas PDF Export dans la liste)
+ Cherchez et choisissez **PDF Export** (processing foundation), puis cliquez sur **Install**. Fermer ensuite la boîte de dialogue.
+ Importez la librairie dans votre Sketch ( **Sketch** > **Importer une librairie** > Choix dans la liste )

Le PDF étant un format vectoriel, il aura l'avantage de pouvoir être exploité et retravaillé directement dans un logiciel de dessin vectoriel tels Illustrator ou Inkscape.

Les différentes façons d'enregistrer vos images sont détaillées, très clairement ici:

[https://processing.org:8443/reference/libraries/pdf/index.html](https://processing.org:8443/reference/libraries/pdf/index.html)

[https://processing.org/reference/libraries/pdf/](https://processing.org/reference/libraries/pdf/)

Voir aussi la section INPUT dans la [référence Processing](https://processing.org/reference/)

<a data-fancybox title="" href="/assets/pdfcap.png">![](/assets/pdfcap.png)</a>

```processing
import processing.pdf.*;

int x1;
int y1;
int x2;
int y2;
int diametre = 10;

void setup() {
    size(800,600);
    beginRecord(PDF, "lignes.pdf");
    strokeWeight(0.5);
}

void draw() {
    fill(random(255),random(255),random(255));
    x1 = round(random(15,width-15));
    y1 = round(random(15,height-15));
    x2 = round(random(15,width-15));
    y2 = round(random(15,height-15));

    stroke(0,180);// valeur de gris, opacité
    line(x1,y1,x2,y2);
    
    noStroke();
    ellipse(x1,y1,diametre,diametre);
    ellipse(x2,y2,diametre,diametre);
}
void keyPressed() {
    endRecord();
    stop();
}
```

Dans le code ci-dessus, les fonctions importantes concernant l'enregistrement de votre PDF sont les suivantes:
```processing
    import processing.pdf.*; //Importe la librairie dans votre sketch. A ajouter hors du setup()
    beginRecord(PDF, "lignes.pdf"); //Commence à enregistrer vos tracés à partir de cette ligne
    endRecord(); //Stoppe l'enregistrement et termine le fichier
```


---

Le fichier généré pourrait très bien être ensuite exploité dans Illustrator ou Inkscape...

<a data-fancybox title="" href="/assets/capdf1.png">![](/assets/capdf1.png)</a>

<a data-fancybox title="" href="/assets/capdf2.png">![](/assets/capdf2.png)</a>

<a data-fancybox title="" href="/assets/capdf3.png">![](/assets/capdf3.png)</a>