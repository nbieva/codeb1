---
title: Interactivité souris
lang: fr-FR
---

# Interactivité souris

## Avec la souris

<a data-fancybox title="Pointer, pointer" href="/assets/mouse.png">![Pointer, pointer](/assets/mouse.png)</a>
Jonathan Puckey, [*Pointer, pointer*](https://pointerpointer.com/)

Pointer Pointer is an experiment in interaction, a celebration of the disappearing mouse pointer and a rumination on Big Data.
(+ https://www.youtube.com/watch?v=Z2ZXW2HBLPM )

## Un outil de dessin

```processing
void setup() {
    // dimension du canvas
    size(800,600);
    // fond blanc
    background(255);
}

void draw(){
    // on dessine une ellipse dont les positions x et y correspondent à la position de la souris.
    ellipse(mouseX, mouseY, 20, 20);
}
```

## Changer de couleur au clic

```processing

void setup() {
    size(600, 600);
    background(255);
}

void draw(){
}

// fonction détectant si l'on clique sur la souris
void mouseClicked() {
    background(random(255), random(255), random(255)); // on change la couleur du fond aléatoirement
}

```

## Déplacer des éléments

```processing
float bx; // largeur de ma box
float by; // hauteur de ma box
int boxSize = 75; // taille de ma box
boolean overBox = false; // variable pour définir si la souris survol la box
boolean locked = false; // variable pour définir si l'on a selectionné la box
// variables utilisées pour définir la position du carré
float xOffset = 0.0;
float yOffset = 0.0;

void setup() {
    size(600, 600);
    // on défini que le rectangle se dessinera à partir du centre
    rectMode(RADIUS);
}

void draw() {
    background(0);
    // Test si le cursor est sur la box
    if (mouseX > bx-boxSize && mouseX < bx+boxSize &&
        mouseY > by-boxSize && mouseY < by+boxSize) {
        overBox = true; // on change la variable pour indiquer que la box est survolée
    } else {
        overBox = false;
    }
    // Draw the box
    stroke(153);
    fill(153);
    rect(bx, by, boxSize, boxSize);
}

// fonction détectant si l'on clique sur la souris
void mousePressed() {
    if(overBox == true) { // on vérifie l'état de la variable
        locked = true; // indique que l'utilisateur a cliqué
    } else {
        locked = false;
    }
    // on défini la position du rectangle en fonction de la souris
    xOffset = mouseX-bx;
    yOffset = mouseY-by;
}

// fonction pour détecter le déplacement de la souris si elle est cliquée
void mouseDragged() {
    if(locked) { // vérifier que l'utilisateur a cliqué
        // mise à jour des variables déterminant la position du rectangle
        bx = mouseX-xOffset;
        by = mouseY-yOffset;
    }
}

// fonction pour détecter que l'utilisateur à relaché le clique
void mouseReleased() {
    locked = false;
}
```