---
title: Travailler avec du texte
lang: fr-FR
---

# Travailler avec du texte

>Source pour la 1ere partie: [Floss manuals](https://fr.flossmanuals.net/processing/la-typographie/)

Le travail du texte dans Processing peut vous mener très loin. Cette page reprend la base de la fonction text() (et fonctions annexes), ainsi qu'une série de ressources et tutoriels plus avancés, mais pouvant être mis en oeuvre assez rapidement.
En effet, à l'instar de ce qui se fait pour le traitement de la video, l'export PDF ou la création d'interfaces utilisateur, une série de bibliothèques (libraries) proposent un grand nombre de fonctions avancées pour la manipulation du texte.

<a data-fancybox title="" href="/assets/typo5.png">![](/assets/typo5.png)</a>

> [Type + Code: Processing For Designers](https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn) (Published on May 29, 2009)  
> 
By Yeohyun Ahn and Viviana Cordova. Type + Code, explores the aesthetic of experimental code driven typography, with an emphasis on the programming language Processing which was created by Casey Reas and Ben Fry.

----

Une entrée en matière : https://processing.org/tutorials/typography/

Sachez tout d'abord que votre texte est dessiné dans votre espace de travail. Il n'est pas éditable en tant que tel (un petit peu comme si vous l'aviez vectorisé dans un logiciel de dessin vectoriel comme Illustrator).
La façon la plus simple d'afficher du texte dans Processing est la suivante:

```processing
void setup() {
    size(300,200);
    fill(200,0,0);//Plutôt rouge
    textSize(24);
    text("Un texte", 10, 30);
}

```


> **Note:** Le contenu repris ci-dessous est extrait de la page correspondante dans les FLOSS Manuals (fr), que vous pourrez trouver ici.

Ce chapitre va vous permettre de personnaliser l'usage des textes dans Processing en utilisant des polices de caractères alternatives.

## La forme des mots
Si nous voulons dessiner avec une autre forme typographique que celle définie par défaut, il faut effectuer quelques étapes préalables :
+ Convertir une police de caractères en un format de fichier compatible avec Processing ;
+ importer ce fichier dans le code du programme (ce fichier comporte toutes les informations graphiques décrivant l'apparence de la police utilisée) ;
+ sélectionner cette police et l'activer dans notre programme ;
+ dessiner du texte dans notre sketch à l'aide des instructions appropriées pour que le résultat s'affiche dans la fenêtre de visualisation de Processing.

## Importer une police de caractères

Pour dessiner du texte dans notre fenêtre de visualisation, il faut choisir tout d'abord son apparence, en indiquant sa police de caractères. Pour bien réussir cette étape, nous vous recommandons de sauvegarder d'abord votre sketch dans votre dossier de travail (voir chapitre Bases de Processing). Une fois notre sketch sauvegardé, nous allons sélectionner, dans le menu Tools, l'action Create Font...

<a data-fancybox title="Create Font" href="https://fr.flossmanuals.net/processing/la-typographie/static/v3_Processing_menu_sketch_generer_police_600px.png">![Create Font](https://fr.flossmanuals.net/processing/la-typographie/static/v3_Processing_menu_sketch_generer_police_600px.png)</a>

A priori nous devrions maintenant voir une fenêtre Create Font qui permet de convertir quasiment n'importe quelle police de caractère en une forme utilisable dans notre sketch. Cette police doit être installée au préalable dans notre ordinateur pour qu'elle apparaisse dans cette liste.

<a data-fancybox title="Liste" href="https://fr.flossmanuals.net/processing/la-typographie/static/Processing-Typographie-window_create_font_georgia_italic_numeroter-fr-old.png">![Liste](https://fr.flossmanuals.net/processing/la-typographie/static/Processing-Typographie-window_create_font_georgia_italic_numeroter-fr-old.png)</a>

Cette fenêtre est décomposée en quatre parties :
+ La liste des polices actuellement installées sur notre ordinateur,
+ Une prévisualisation de la police actuellement sélectionnée, affichée à la taille indiquée dans le prochain champ (la zone numérotée 3 dans la copie d'écran ci-dessus),
+ À peu près la taille maximale à laquelle nous voulons dessiner cette police,
+ Le nom du fichier de cette police, une fois convertie dans le format natif de Processing (.vlw).

Vous avez peut-être noté qu'il existe également une case à cocher **Smooth** qui active/désactive l'antialiasing (fonction de lissage des polices pour éviter un effet de crénelage), ainsi qu'un bouton **Characters**... qui permet de préciser les caractères spéciaux qui doivent être inclus lors de la conversation de la police. 
Pour ne pas compliquer les choses, nous allons laisser ces deux options avec leurs valeurs par défaut.

Dans l'illustration ci-dessus nous avons sélectionné la police Georgia. C'est à partir du nom de cette police et de sa taille que Processing générera le fichier de la police à importer, ex : « Georgia-Italic-48.vlw ». Notons enfin que l'extension « .vlw » associée à l'intitulé du fichier sera rajoutée à toutes les polices que nous importerons de cette manière. 

> Si par curiosité vous vous intéressez à l'origine de cette extension, son nom fait référence sous forme d'un acronyme au *« Visual Language Workshop »* (vlw) du MIT Media Lab. C'est ce laboratoire qui historiquement est à l'origine d'un certain nombre de principes et de travaux qui ont permis à Processing de voir le jour.

Si nous voulons savoir où Processing a sauvegardé notre police, il suffit de sélectionner, dans le menu Sketch, l'action Show Sketch Folder.
Cette action fera apparaître le dossier « **data** » dans lequel notre police a été sauvegardée. Son fichier s'appelle « Georgia-Italic-48.vlw ». C'est ce nom que nous devons retenir pour intégrer la police dans notre programme.

<a data-fancybox title="Data" href="https://fr.flossmanuals.net/processing/la-typographie/static/Processing-Typographie-folder_data_font_vlw_1-fr-old.png">![Data](https://fr.flossmanuals.net/processing/la-typographie/static/Processing-Typographie-folder_data_font_vlw_1-fr-old.png)</a>

## Dessiner une phrase
Nous allons enfin dessiner avec notre police. Pour cela, il faut faire trois choses :

1. Importer le fichier Georgia-Italic-48.vlw dans une variable afin que notre programme connaisse le nom de la police utilisée et sache la dessiner lorsqu'il affichera du texte. Ce fichier contient en effet les informations décrivant la structure géométrique de la police pour pouvoir la reproduire ;

2. Sélectionner cette variable dans notre programme comme police active ;

3. Dessiner un caractère ou une phrase quelque part dans notre sketch à l'aide des instructions appropriées pour le voir ensuite s'afficher dans la fenêtre de visualisation de Processing.
En option, il est possible de choisir la taille à laquelle nous voulons dessiner avec notre police, mais comme nous avons déjà paramétré cet aspect lors de la création du fichier, il ne sera pas nécessaire de l'indiquer ici.

Voici le code complet d'un programme simple qui réunit toutes ces étapes pour dessiner une phrase dans la fenêtre de visualisation. Par tradition, nous allons faire nos premiers pas dans l'écriture en écrivant « Salut tout le monde ! ».

```processing
void setup() {
    size(500,150);
    PFont police;
    police = loadFont("Georgia-Italic-48.vlw");
    textFont(police,48);
    text("Salut tout le monde !", 20, 75);
}
```

Tout d'abord, nous avons fixé la taille de notre fenêtre de visualisation (l'espace de dessin), comme dans quasiment n'importe quel programme Processing.
Ensuite, nous avons importé notre fichier dans une variable Processing (dénommée police). 
A quoi sert une variable ? Et bien dans ce cas précis, il s'agit d'un nom interne à notre programme qui fait référence au fichier de la police Georgia-Italic-48.vlw que nous souhaitons utiliser. A chaque fois que nous écrirons par la suite le mot police dans notre programme, Processing comprendra qu'il s'agit de faire appel à la police Georgia Italic 48 contenue désormais dans ce mot.

Vous avez peut-être également noté un mot étrange en tout début de cette phrase, le mot **PFont**. Celui-ci **indique à Processing le type de la variable** et lui permettra d'ouvrir assez de mémoire pour contenir l'ensemble des données de ce genre. Dans le cas de n'importe quelle variable, la syntaxe à utiliser est **{type de la variable} {nom de la variable} = {les valeurs de la variable}**.

Par exemple, si par un tour de force il était possible d'importer un petit chaton tout mignon dans Processing il suffirait d'écrire petitChaton mioumiou = loadPetitChaton("mioumiou.chat"); On écrit d'abord le type de la chose, le nom de la chose, et enfin on lui donne sa valeur. Ici, cette valeur est donnée par la fonction loadFont() qui va aller chercher la structure géométrique de la police dans le fichier et l'importera dans notre variable nommée « police ».

La suite de notre programme est probablement un peu plus intuitive. Nous sélectionnons la police avec laquelle nous voulons dessiner. Même s'il n'existe qu'une seule police actuellement dans notre programme, il faut néanmoins passer par cette étape. 
Notez que vous pouvez importer autant de polices que vous voulez et passer de l'un à l'autre, à l'image de ce qu'il est possible de faire avec les couleurs.

Dans cet exemple, nous avons indiqué la taille de la police juste pour vous montrer qu'il est possible de la changer en cours de route.
Enfin, nous dessinons une petite phrase et indiquons la position {x,y} où notre texte doit se dessiner. 
On obtient le résultat suivant :

<a data-fancybox title="salut" href="https://fr.flossmanuals.net/processing/la-typographie/static/Processing-Typographie-sketch_texte_saluttoutlemonde-fr-old.png">![salut](https://fr.flossmanuals.net/processing/la-typographie/static/Processing-Typographie-sketch_texte_saluttoutlemonde-fr-old.png)</a>

## Point d'origine
Pour rendre plus clair le rapport entre la position {x,y} de notre message et sa forme typographique, nous avons également dessiné dans l'illustration ci-dessus une petite croix pour rendre plus explicite la façon dont Processing positionne l'écriture :

```processing
void setup() {
    size(500,150);
    PFont police;
    police = loadFont("Georgia-Italic-48.vlw");
    textFont(police);
    text("Salut tout le monde !", 20, 75);
    // indiquer la position d'origine du texte
    stroke(255,0,0);
    line(15,70,25,80);
    line(15,80,25,70);
}
```

Tout comme les rectangles, qui peuvent se dessiner depuis leur point supérieur gauche, depuis leur centre, ou depuis ses quatre extrémités, **l’écriture du texte peut également être positionnée à partir de plusieurs points d'origine**. 
Par défaut, le texte s'écrit depuis la ligne de base du texte, c'est-à-dire le point en bas à gauche du texte, mais au-dessus des caractères descendants comme les lettres « y » ou « j ».
Vous pouvez changer la façon dont Processing alignera ce texte, en se servant de la fonction **textAlign()**:

<a data-fancybox title="Text align" href="https://fr.flossmanuals.net/processing/la-typographie/static/Processing-Typographie-textAlign-fr-old.png">![Text align](https://fr.flossmanuals.net/processing/la-typographie/static/Processing-Typographie-textAlign-fr-old.png)</a>

```processing
void setup() {
    size(500,250);
    PFont police;
    police = loadFont("SansSerif-24.vlw");
    textFont(police,24);
    line(250,0,250,500);
    line(0,125,500,125);
    textAlign(RIGHT,TOP);
    text("right+top", 250, 125);
    textAlign(RIGHT,BASELINE);
    text("right+baseline", 250, 125);
    textAlign(LEFT,CENTER);
    text("left+center", 250, 125);
}
```

## Typographie: Pour aller plus loin

+ [http://laikafont.ch/](http://laikafont.ch/)
+ [**http://basiljs.ch/about/**](http://basiljs.ch/about/)
+ [**https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn**](https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn)
+ [https://www.openprocessing.org/sketch/161029](https://www.openprocessing.org/sketch/161029)
+ [http://code.andreaskoller.com/libraries/fontastic/](http://code.andreaskoller.com/libraries/fontastic/)
+ [http://www.ricardmarxer.com/geomerative/](http://www.ricardmarxer.com/geomerative/)
+ [https://www.plagiairc.com/](https://www.plagiairc.com/)
+ [http://brunoimbrizi.com/unbox/2011/10/processing-typography/](http://brunoimbrizi.com/unbox/2011/10/processing-typography/)
+ [https://www.creativeapplications.net/processing/generative-typography-processing-tutorial/](https://www.creativeapplications.net/processing/generative-typography-processing-tutorial/)
+ [**http://jk-lee.com/aerial-bold-project/**](http://jk-lee.com/aerial-bold-project/)

<a data-fancybox title="typo" href="/assets/typo1.png">![typo](/assets/typo1.png)</a> 
Dans [*Generative Typography with Processing – Tutorial*](https://www.creativeapplications.net/processing/generative-typography-processing-tutorial/), written by **Amnon Owed**

[Type + Code: Processing For Designers](https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn) (Published on May 29, 2009)  
By Yeohyun Ahn and Viviana Cordova. Type + Code, explores the aesthetic of experimental code driven typography, with an emphasis on the programming language Processing which was created by Casey Reas and Ben Fry.