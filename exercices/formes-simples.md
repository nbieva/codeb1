---
title: Formes simples
lang: fr-FR
---

# Formes simples

Commencez par imaginer ce que vous aimeriez réaliser.
Commencez par quelque chose de simple, utilisant un registre de formes géométriques simples.
Vous pouvez créer un drapeau, une affiche, un logo.. La taille de votre premier sketch doit être de 900px par 600px.

+ Sur le template qui vous a été remis, tracez un projet
+ **Notez les coordonnées** de vos points
+ imaginez les valeurs de couleur à entrer
+ **pensez à l'ordre dans lequel vous allez dessiner les éléments**
+ Imaginez un élément qui doive suivre votre souris
+ Certaines de vos formes peuvent ne pas avoir de contour, d'autres oui.
+ Vous pouvez également jouer sur l'opacité ou les [modes de fusion](https://processing.org/reference/blendMode_.html)
+ N'hésitez pas à également utiliser des [formes libres](https://processing.org/reference/beginShape_.html).
+ [Exportez votre image au format PNG et/ou PDF](https://code.artsnum.be/part7/exports.html). (Si PDF, essayez de l'ouvrir dans Illustrator pour voir ce qu'il en est. Inspectez en mode tracés)

Pour avoir une idée des coordonnées de vos points, vous pouvez également utiliser le petit outil suivant : [http://mousehover.net/coordonnees/](http://mousehover.net/coordonnees/)

<a data-fancybox title="ex0" href="/assets/ex0.jpg">![ex0](/assets/ex0.jpg)</a>

#### Notions mises en oeuvre

* noLoop() ou tout sans le Setup (pour la première partie)
* Canevas et coordonnées
* Formes simples
* Couleur (remplissage et contours)
* Eventuellement [transformations](https://fr.flossmanuals.net/processing/les-transformations/)

#### Les fonctions les plus utiles ici seront probablement :

+ fill() et noFill()
+ stroke() et noStroke()
+ strokeWeight
+ RectMode()
+ ellipseMode()
+ rect(), ellipse(), line(), point()
+ beginShape(), vertex(), endShape()
+ println()
+ save()

Utilisez le menu ci-contre, ou [la référence](https://processing.org/reference/) processing, pour trouver plus de détails à propos de ces fonctions.

Vous utiliserez déjà probablement les variables suivantes: width, height, frameCount, mouseX, mouseY

## Exemple

```processing
void setup() {
    size(800,500);
    background(255);
    noLoop();
    noStroke();

    //Carrés rouges
    fill(255,0,0); // fill(rouge, vert, bleu)
    rect(0, 0, 100,100);
    rect(width-100, 0, 100,100);
    rect(width-100, height-100, 100,100);
    rect(0, height-100, 100,100);

    //Rectangles noirs
    fill(0);
    rect(200,0,400,100);
    rect(200,height-100,400,100);

    //Triangles jaunes
    fill(250,230,0);
    triangle(200, 100, 600, 100, 600, 300);
    triangle(200, height-100, 200, height-300, 600, height-100);
}
```
<a data-fancybox title="" href="/assets/flag.png">![](/assets/flag.png)</a>

* [Mickey](http://www.damienhirst.com/mickey), Damien Hirst

----

<a data-fancybox title="" href="/assets/mickey.png">![](/assets/mickey.png)</a>
