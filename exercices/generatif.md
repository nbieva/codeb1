---
title: Générez des formes
lang: fr-FR
---

# Générez des formes

<a data-fancybox title="" href="https://sdl-assets.ams3.cdn.digitaloceanspaces.com/2018/09/sdl-web-venice-bienalle-01-2000x1210.gif">![](https://sdl-assets.ams3.cdn.digitaloceanspaces.com/2018/09/sdl-web-venice-bienalle-01-2000x1210.gif)</a>

Sur base de votre premier agencement de formes simples, créez un programme qui génère à chaque boucle un autre agencement aléatoire \(à l'aide des variables et de fonctions comme [random](https://processing.org/reference/random.html) par exemple.

Chaque item doit être sauvé au format PNG. Veillez à générer des noms de fichiers différents à chaque boucle. Dans le cas contraire, Processing écrasera systématiquement vos anciens fichiers avec le dernier.
La variable **frameCount** est très pratique pour cela. Elle sera automatiquement augmentée de 1, à chaque boucle. Elle peut donc être utilisée comme une sorte de compteur dans les noms de vos fichiers.

<a data-fancybox title="" href="/assets/pape01.png">![](/assets/pape01.png)</a>

<a data-fancybox title="" href="/assets/pape2.png">![](/assets/pape2.png)</a>
[Lygia Pape, Livro do tempo](http://www.from-paris.com/lygia-pape-livro-do-tempo-serpentine-gallery-expo/), 1961-63

## Fonctions principales

[Référence](https://processing.org:8443/reference/)

## Un exemple (pas un modèle)

```processing
/* Générateur de formes */
float x1,y1,x2,y2,x3,y3,x4,y4,decalage;
int cote = 100;

void setup() {
  size(740,370);
  background(255);
  noStroke();
  frameRate(1);
  decalage = cote/2;
}

void draw() {
  //On place un fond gris clair à chaque boucle
  fill(240);
  rect(0,0,width,height);
  
  //On déplace a forme au centre du sketch
  translate(width/2-cote/2,height/2-cote/2);
  
  //On calcule les nouvelles valeurs des variables
  x1 = 0+random(-decalage,decalage);
  y1 = 0+random(-decalage,decalage);
  x2 = cote+random(-decalage,decalage);
  y2 = 0+random(-decalage,decalage);
  x3 = cote+random(-decalage,decalage);
  y3 = cote+random(-decalage,decalage);
  x4 = 0+random(-decalage,decalage);
  y4 = cote+random(-decalage,decalage);
  
  //On définit une couleur de remplissage (qui pourrait être différente à chaque boucle)
  fill(240,50,10,200);
  
  //On dessine la forme (qui pourrait d'ailleurs être bien plus complexe que ceci)
  beginShape();
    vertex(x1,y1);
    vertex(x2,y2);
    vertex(x3,y3);
    vertex(x4,y4);
  endShape(CLOSE);
  
  //La ligne suivante, décommentée, exportera un fichier JPG de chaque forme dans le dossier du sketch, à chaque boucle donc.
  //saveFrame("maForme-#####.jpg");
}
```

<a data-fancybox title="" href="/assets/martens.png">![](/assets/martens.png)</a>

> Le travail de [Karel Martens](https://www.google.com/search?q=karel+martens&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjH0sq0mYflAhWNa1AKHU9UAGwQ_AUIEigB&biw=1438&bih=1051) est une excellente source d'inspiration.

## Références

* [Stockholm Media Lab, 53rd Venice Biennale](http://www.stockholmdesignlab.se/venice-biennale/)
* [Lygia Pape, Livro do tempo](https://s-media-cache-ak0.pinimg.com/originals/d6/10/86/d610860dd92f473c5bc702959bc386f8.jpg)




