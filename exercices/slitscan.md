---
title: Slit scanning
lang: fr-FR
---

# Slit scanning

[<a data-fancybox title="" href="/assets/slit2.png">![](/assets/slit2.png)</a>](https://www.google.be/search?q=slit+scan&rlz=1C5CHFA_enBE766BE767&source=lnms&tbm=isch&sa=X&ved=0ahUKEwimzK_gjdLZAhVlI8AKHYC7AJkQ_AUICigB&biw=1864&bih=1343)

Le slitscanning est un procédé video qui, plutôt que d'enregistrer à chaque image l'image entière, va enregistrer une tranche de cette même image (par exemple une tranche de 1px) et, plutôt que d'en afficher 30 par secondes, va les placer côte à côte dans un nouveau format.

La première image de ce type date du début du siècle dernier. Vous trouverez de nombreuses références sur ce procédé sur [cette page](http://www.flong.com/texts/lists/slit_scan/) de Golan Levin.

Le cas échéant, prenez 10 minutes de votre temps pour suivre ce [petit tuto de Daniel Shiffman](https://www.youtube.com/watch?v=WCJM9WIoudI) sur le sujet.

Aussi cette petite expérimentation, d'[Adrien Mondot](https://www.am-cb.net/projets/cinematique), qui était venu il y a quelques années présenter son logiciel eMotion lors d'un workshop de 3 jours ici à La Cambre.
> [Le site d'adrien Mondot et Claire Bardaine](https://www.am-cb.net/)

[<a data-fancybox title="" href="/assets/vimeo.png">![](/assets/vimeo.png)</a>](https://vimeo.com/7878518)

Ce procédé a été utilisé par un certain nombre d'artistes, comme [Memo Akten](http://www.memo.tv/category/work/by-type/).

## Un sketch de base:

```processing
// Importation de la bibliothèque
import processing.video.*;

Capture myCap;
int X=0;

void setup() {
    size(1280, 960);
    //Initialisation de la camera
    myCap = new Capture(this, 640, 480);
    myCap.start();
}

void draw() {
    if (myCap.available()) {
        myCap.read();
        myCap.loadPixels();
        copy(myCap, (myCap.width/2), 0, 1, myCap.height, (X++%width), 0, 1, height);
    }
}

void keyPressed() {
    save("monSlitScan-"+day()+"-"+hour()+"-"+minute()+"-"+second()+".jpg");
}
```