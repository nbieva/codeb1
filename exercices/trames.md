---
title: Trames et motifs
lang: fr-FR
---

# Trames et motifs

## Un damier simple

```processing
int posX = 0;
int posY = 0;
int cote = 100;
boolean inverter;

void setup() {
    size(1200,880);
    noStroke();
}

void draw() {
    if (inverter) {
        fill(255);
    } else {
        fill(random(0,200));
    }
    rect(posX, posY, cote, cote);
    posX += cote;
    inverter = !inverter;
    if (posX >= width) {
        posX = 0;
        posY += cote;
        inverter = !inverter;
    }
}

```

<a data-fancybox title="" href="/assets/damier.png">![](/assets/damier.png)</a>

## Code commenté

```processing
// On crée une variable qui correspondra à notre position sur l'axe des X. On lui donne la valeur de départ "0" (à gauche)
int posX = 0;

// On crée une variable qui correspondra à notre position sur l'axe des Y. On lui donne la valeur de départ "0" (en haut)
int posY = 0;

// On défini le côté de nos carrés à l'aide d'une variable qu'on appelle, par exemple "cote"
int cote = 100;

// On crée ici une variable booléenne (de type "vrai ou faux") qui nous servira à intervertir les couleurs de nos carrés. Elle est vraie par défaut.
boolean inverter;

void setup() {
    size(1200,900);
    noStroke(); //On désactive les contours
}

void draw() {
    if (inverter) { // si "inverter" est VRAI (ce qui est le cas par défaut)
        fill(255); // Remplir de blanc
    } else { // Dans les autres cas..
        fill(random(0,200)); //.. remplir d'une valeur aléatoire comprise entre 0 et 200.
    }
    rect(posX, posY, cote, cote);
    posX += cote; //On augmente la position en X de la valeur d'un côté du carré
    //inverter égale le contraire d'inverter. Si elle est fausse, elle devient vraie. Si elle est vraie, elle devient fausse.
    inverter = !inverter;
    // Si la position en X dépasse ou est égale à la largeur du sketch, on passe à la ligne...
    if (posX >= width) {
        posX = 0; // En ramenant X à 0 (bord gauche)
        posY += cote; //En augmentant la position en Y de la hauteur d'un carré
        inverter = !inverter; // On inverse également la couleur. Commentez cette ligne( // ) si nombre impair..
    }
}

```

Avec des dimensions "dynamiques"

```processing
int posX = 0;
int posY = 0;
int div = 100;
float coteH, coteL;
boolean inverter;

void setup() {
    size(1200,880);
    noStroke();
    coteL = width/div;
    coteH = height/div;
}

void draw() {
    if (inverter) {
        fill(255);
    } else {
        fill(random(0,200));
    }
    rect(posX, posY, coteL, coteH);
    posX += coteL;
    inverter = !inverter;
    if (posX >= width) {
        posX = 0;
        posY += coteH;
        inverter = !inverter;
        if (posY >= height) {
          posY = 0;
        }
    }
}
```

**XR6:** Ceéez des variantes de ce damier en modifiant les valeurs des variables, la couleur, l'opacité, la souris, l'aléatoire (random, noise..). Voir ci-dessous.

<iframe class="p5embed" width="740" height="310" src="https://editor.p5js.org/nicobiev/embed/rZ4n_-FDS"></iframe>

<a data-fancybox title="" href="http://rhuthmos.eu/IMG/jpg/8aurelie_nemours.jpg">![](http://rhuthmos.eu/IMG/jpg/8aurelie_nemours.jpg)</a>

[Aurélie Nemours](https://www.google.be/search?bih=825&biw=1913&tbm=isch&sa=1&ei=HzG6W7bQI4mysAeuurPACA&q=aur%C3%A9lie+nemours&oq=aur%C3%A9lie+nemours&gs_l=img)

Après avoir réalisé un damier simple, tentez d'utiliser les boucles pour créer, sur base d'une grille, une série de variantes.
N'oubliez pas que vous pouvez chevaucher vos éléments.
Vous pouvez aussi varier les modes de fusion en utilisant la fonction blendMode().
N'oubliez pas d'exporter vos images pour garder des traces de vos essais.

## Part 1
A partir de ce que nous avons vu, créez un damier de 800x800px.
Vous devrez pour ce faire utiliser les variables et les conditions.

## Part 2
Une fois la logique déterminée, poussez le travail plus loin. Comment pourriez-vous faire évoluer/animer/modifier ce damier?
Songez à décaler vos éléments, et à explorer les [possibilités de transparence](https://processing.org/reference/blendMode_.html) qu'offre Processing..
Créez entre 1 et 3 versions et exportez-les au format PNG.
Conservez, pour chacune le code de votre sketch de façon à pouvoir y revenir si nécessaire.
Utilisez la référence Processing (et le web)

## Part 3
Dans votre premier damier, un carré est ajouté à chaque boucle draw(). Comment pourriez-vous vous y prendre pour que le damier apparaisse en une seule fois.

[Les répétitions(boucles)](https://code.artsnum.be/part2/boucles.html)

## Caractéristiques techniques
- Format de 800x800px
- Exports PNG

## Concepts et fonctions utilisés:
+ Notions précédentes (livre do tempo)
+ Draw(), la boucle
+ Fonctions
+ Variables
+ Boucles (et éventuellement boucles impriquées)
+ Conditions
+ Export au format PNG (save() )

## Fonctions principales
size()
rect()
fill()
stroke()
noStroke()
strokeWeight()
random()


## Références

+ [Bauhaus textiles](https://www.google.be/search?q=bauhaus+textiles&espv=2&biw=1784&bih=1320&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwiQroqMoMzQAhWL0hoKHQkQAPEQsAQIHQ)
+ [Aurélie Nemours](https://www.google.be/search?q=aur%C3%A9lie%20nemours&tbm=isch&tbs=rimg%3ACb-80AQDLBKvIjhYmeFH1CHasMhogVyD2QoicaynEl_1hcYpcrfGcdSCvblt1zmANkG0t3yCd1g0PjT3qIzSJoBwrCyoSCViZ4UfUIdqwEaI3m5k_11sU7KhIJyGiBXIPZCiIRPzjwBT5WvhkqEglxrKcSX-FxihGup-CpwVOaWSoSCVyt8Zx1IK9uEWiu8lx6x9qDKhIJW3XOYA2QbS0Rx5wb4o-yfqoqEgnfIJ3WDQ-NPRERXU90OZq3ESoSCeojNImgHCsLEbrClHF_1_1FGL&tbo=u&bih=803&biw=1295&ved=0ahUKEwjG89n97fDPAhUD1xoKHUjFBacQ9C8ICQ&dpr=1#imgrc=_)
+ Damien Hirst, [Spot paintings](https://www.google.be/search?q=damien+hirst&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiUppPmoKvSAhULB8AKHZYRAZAQ_AUICCgB&biw=2266&bih=1194#tbm=isch&q=damien+hirst+spot+paintings&*)

## Variations

<a data-fancybox title="" href="https://github.com/lam-artsnum/code/wiki/images/grid-01.png">![](https://github.com/lam-artsnum/code/wiki/images/grid-01.png)</a>

<a data-fancybox title="" href="https://github.com/lam-artsnum/code/wiki/images/grid-02.png">![](https://github.com/lam-artsnum/code/wiki/images/grid-02.png)</a>

<a data-fancybox title="" href="https://github.com/lam-artsnum/code/wiki/images/grid-03.png">![](https://github.com/lam-artsnum/code/wiki/images/grid-03.png)</a>

<a data-fancybox title="" href="https://github.com/lam-artsnum/code/wiki/images/grid-04.png">![](https://github.com/lam-artsnum/code/wiki/images/grid-04.png)</a>

<a data-fancybox title="" href="https://github.com/lam-artsnum/code/wiki/images/grid-05.png">![](https://github.com/lam-artsnum/code/wiki/images/grid-05.png)</a>

<a data-fancybox title="" href="https://github.com/lam-artsnum/code/wiki/images/grid-06.png">![](https://github.com/lam-artsnum/code/wiki/images/grid-06.png)</a>

<a data-fancybox title="" href="https://github.com/lam-artsnum/code/wiki/images/grid-07.png">![](https://github.com/lam-artsnum/code/wiki/images/grid-07.png)</a>

## Une grille simple

```processing
int grid = 10; // écartement entre les lignes

void setup() {
    // dimension du canvas
    size(800,600);
    // fond blanc
    background(255);

    // grille verticale
    for (int i = 10; i < width; i+=grid) { // on incrémente i de la valeur d'écartement définie à la première ligne
        line (i, 0, i, height); // chaque ligne se positionne en x selon la valeur de i
    }
    // grille horizontale
    for (int i = 10; i < height; i+=grid) { // on incrémente i de la valeur d'écartement définie à la première ligne
        line (0, i, width, i); // chaque ligne se positionne en y selon la valeur de i
    }
}

```

<a data-fancybox title="" href="/assets/grille.png">![](/assets/grille.png)</a>

## Un peu plus "flexible"..

A peu près la même chose, mais dans le sketch ci-dessous, on remplace la valeur initiale de "i" par la variable "grid". De cette manière vous pourrez plus aisément modifier la taille de vos carrés. La grille se mettra à jour.


```processing
int grid = 10;

void setup() {
    size(800,600);
    background(255);

    // On remplace la valeur initiale de "i" par la variable "grid"
    for (int i = grid; i < width; i+=grid) {
        line (i, 0, i, height);
    }
    // grille horizontale
    for (int i = grid; i < height; i+=grid) {
        line (0, i, width, i);
    }
}

```
