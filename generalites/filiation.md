---
title: Filiation
lang: fr-FR
sidebarDepth: 1
---

<p class="lead">Cette page, bien entendu non-exhaustive, reprend quelques jalons en lien avec Processing et la programmation.</p>


## Claude Chappe

<a data-fancybox title="" href="/assets/chappe_code.jpg">![](/assets/chappe_code.jpg)</a>
[Plus d'info](https://www.google.com/search?q=t%C3%A9l%C3%A9graphe+chappe&rlz=1C5CHFA_enBE752BE753&source=lnms&tbm=isch&sa=X&ved=0ahUKEwinivWMpNbgAhXGPOwKHSCCBFkQ_AUIDigB&biw=1440&bih=766)

## Métier à tisser Jacquard

Le design textile entretient des liens très étroits avec le monde et les logiques de la programmation, à la fois techniquement et conceptuellement.
Le métier Jacquard est souvent cité comme étant la première machine "programmable" capable de reproduire des motifs, en exécutant une série d'instructions dans un ordre précis.

+ https://www.youtube.com/watch?v=HDROcn_0ZbI
+ https://fr.wikipedia.org/wiki/M%C3%A9tier_Jacquard

<a data-fancybox title="" href="/assets/jacquard.png">![](/assets/jacquard.png)</a>
<a data-fancybox title="" href="/assets/workshop-group2.017.jpeg">![](/assets/workshop-group2.017.jpeg)</a>


## Anni Albers

Anni Albers, Study for Camino Real, 1967. Gouache on blueprint graph paper.
Photo: Tim Nighswander

<a data-fancybox title="" href="/assets/anni-albers3.jpeg">![](/assets/anni-albers3.jpeg)</a>

<div class="legende">
<p>À gauche:  métier à tisser manuel , Mexique, env. 1930. Galerie d'art de l'université de Yale, collection commémorative Harriet Engelhardt, don de Mme Paul Moore. Droite:  Figurine féminine debout , Mexique, Guanajuato, Chupícuaro, 400–100 av. J.-C., Musée d'histoire naturelle de Yale Peabody.
Dans: <a href="https://www.artsy.net/article/artsy-editorial-josef-anni-albers-amassed-1-400-works-south-american-art" target="_blank">Les maîtres du bauhaus Josef et Anni Albers réunis avec obsession pour l'art latino-américain</a>
</p>
</div>

<a data-fancybox title="" href="/assets/bauhaus.png">![](/assets/bauhaus.png)</a>

## John Cage

{{ "https://www.youtube.com/watch?v=gXOIkT1-QWY" | video }}

<a data-fancybox title="" href="/assets/waterwalk.jpg">![](/assets/waterwalk.jpg)</a>

## Sol LeWitt

<a data-fancybox title="" href="/assets/certificate.jpg">![ai](/assets/certificate.jpg)</a>

+ [Numéro 49](https://llimllib.github.io/solving-sol/049/llimllib/index.html)

<a data-fancybox title="" href="/assets/lewitt-strokes-1.png">![](/assets/lewitt-strokes-1.png)</a>
<span class="legende">
Sol LeWitt, Wavy Brushstrokes, Gouache sur papier, 1996</span>


<a data-fancybox title="" href="/assets/lewitt-strokes-2.jpg">![](/assets/lewitt-strokes-2.jpg)</a>
<span class="legende">
Sol LeWitt, Black Curvy Brushstrokes, Aquatinte, 1997
</span>

<a data-fancybox title="" href="/assets/sol_le_witt_open-cubes.jpg">![](/assets/sol_le_witt_open-cubes.jpg)</a>

{{ "https://www.youtube.com/watch?v=RDrHHsez3nU" | video }}

{{ "https://www.youtube.com/watch?v=c4cgB4vJ2XY" | video }}

## Manfred Mohr

<a data-fancybox title="" href="/assets/manfred1.png">![](/assets/manfred1.png)</a>

## Carl Andre

Carl Andre pose des mots sur le papier comme il pose des pièces de métal ou de brique sur le sol. Constituées de lettres entassées dans des blocs de mots assemblés les uns aux autres, ces poèmes, qu'il a écrits depuis les années 1960, se présentent comme des « configurations sculpturales ». Dans la tradition de la poésie concrète, les mots deviennent des entités modulaires, reconfigurables, déplacées et repositionnées dans les limites de l'espace de la feuille de papier afin de créer de nouveaux agencements et motifs, dans une intense interrogation du texte et des mots qui va de pair avec son activité sculpturale, les deux tentatives se nourrissant et se soutenant l'une l'autre très significativement.

Source: [Les Presses du Réel](http://www.lespressesdureel.com/ouvrage.php?id=3286)

[Poemes](https://www.google.com/search?q=carl+andre+poems&source=lnms&tbm=isch&sa=X&ved=0ahUKEwipt-Lkm9TgAhVPMewKHUtSDpcQ_AUIDigB&biw=1577&bih=1289#imgdii=CioLdYvaMQa6gM:&imgrc=BFZiMr7hCenQbM:)

<a data-fancybox title="" href="/assets/carlandrepoems1.jpg">![](/assets/carlandrepoems1.jpg)</a>

## Raymond Queneau & l'Oulipo

« C’est somme toute une sorte de machine à fabriquer des poèmes, mais en nombre limité ; il est vrai que ce nombre, quoique limité, fournit de la lecture pour près de deux cents millions d’années (en lisant vingt-quatre heures sur vingt-quatre). […]
En comptant 45 s pour lire un sonnet et 15 s pour changer les volets, à 8 heures par jour, 200 jours par an, on a pour plus d’un million de siècles de lecture, et en lisant toute la journée 365 jours par an, pour : 190 258 751 années plus quelques plombes et broquilles (sans tenir compte des années bissextiles et autres détails). »
**Raymond Queneau**, Cent Mille Milliards de poèmes

> https://www.youtube.com/watch?v=2NhFoSFNQMQ

<a data-fancybox title="" href="/assets/poemes.png">![](/assets/poemes.png)</a>

<a data-fancybox title="" href="/assets/perec1.jpg">![](/assets/perec1.jpg)</a>

Tableau général des listes
42 x 30 cm
BNF, Arsenal, dépôt G. Perec, 62, 1, 19
 
Ces 42 listes de 10 éléments constituent les matériaux obligatoires du roman : chaque chapitre (un chapitre = une pièce de l'immeuble) doit contenir un élément de chaque liste. Pour répartir de manière non aléatoire dans les différentes pièces de l'immeuble ces 420 éléments, Perec utilise un autre outil mathématique : le "bi-carré latin orthogonal d'ordre 10". Les résultats sont reportés dans son "cahier des charges", soit 99 feuillets (un feuillet = un chapitre) de listes de contraintes.

<a data-fancybox title="" href="/assets/perec3.jpg">![](/assets/perec3.jpg)</a>

**Georges Perec. Notes préparatoires pour La Vie mode d'emploi.** Bi-carré latin orthogonal d'ordre 10 réglant la répartition dans les pièces de l'immeuble (c'est-à-dire aussi dans les chapitres du livre) des éléments des listes « positions » et « activités ». Pour le chapitre lxviii, dont le « cahier des charges » figure dans cet ouvrage, c'est le couple « entrer » et « réparer » que le système a sélectionné (le petit garçon à qui l'on interdit d'entrer, l'accordeur de piano).
(Fonds privé Georges Perec déposé à la bibliothèque de l'Arsenal, 61,104)

## Lawrence Weiner

Importance du texte et de l'instruction.

<a data-fancybox title="" href="/assets/WeinerText.JPG">![](/assets/WeinerText.JPG)</a>
<span class="legende">
Bits & Pieces Put Together to Present a Semblance of a Whole, The Walker Art Center, Minneapolis, 2005.
</span>

## On Kawara

<a data-fancybox title="" href="/assets/onkawara.png">![](/assets/onkawara.png)</a>

<a data-fancybox title="" href="/assets/card31.jpg">![](/assets/card31.jpg)</a>
<span class="legende">On kawara</span>

<a data-fancybox title="" href="/assets/stillalive.png">![](/assets/stillalive.png)</a>

## Opalka

## Art and language

Map of itself ou Map of an area of dimension 12 ‘’ x 12 ‘’ indicating 2,304 ¼ squares, 1967, Art & Language, Collectif (Michael Baldwin & Terry Atkinson)

<a data-fancybox title="" href="/assets/mapofitself.jpg">![](/assets/mapofitself.jpg)</a>


## Muriel Cooper

Muriel Cooper (1925 - 26 mai 1994) était une graphiste, pionnière du design numérique, chercheuse et professeure au MIT Media Lab. Elle a travaillé pendant 6 ans au bureau des publications du MIT. Après avoir obtenu une bourse d'étude Fullbright en Italie, puis monté son propre studio de design, elle est ensuite devenue la première directrice artistique du MIT Press nouvellement créé. 

> https://fr.wikipedia.org/wiki/Muriel_Cooper

<a data-fancybox title="" href="/assets/muriel-cooper.jpeg">![](/assets/muriel-cooper.jpeg)</a>

## Ikko Tanaka

<a data-fancybox title="" href="/assets/tanaka.jpg">![](/assets/tanaka.jpg)</a>

## Paul Rand

<a data-fancybox title="" href="/assets/paul_rand.jpg">![](/assets/paul_rand.jpg)</a>

## Design by numbers
DBN (Design by numbers, par John Maeda, MIT), à l'origine de Processing

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/72611093?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


## Processing
Casey Reas & Ben Fry

<a data-fancybox title="" href="/assets/processing.png">![](/assets/processing.png)</a>

## P5js
Lauren McCarthy

<a data-fancybox title="" href="/assets/p5js.png">![](/assets/p5js.png)</a>

