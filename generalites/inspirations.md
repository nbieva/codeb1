---
title: Exemples
lang: fr-FR
sidebarDepth: 1
---

<p class="lead">Cette page reprend une série de travaux d'artistes faisant intervenir le code à un moment ou un autre dans leurs processus de réflexion, production ou diffusion.</p>

## Schultzschultz

+ [https://www.schultzschultz.com/free-works.html](https://www.schultzschultz.com/free-works.html)

<a data-fancybox title="" href="/assets/schultz.png">![](/assets/schultz.png)</a>

## Textfreebrowsing

TextFreeBrowsing est une extension pour Chrome créée par Rafaël Rozendaal & Jonas Lund. Une fois installée, l'extension supprime tout contenu textuel des pages visitées.

+ [http://textfreebrowsing.com/](http://textfreebrowsing.com/)

<a data-fancybox title="" href="http://textfreebrowsing.com/wikipedia-off.png">![](http://textfreebrowsing.com/wikipedia-off.png)</a>


## Louis Eveillard

Couvertures génératives > https://louiseveillard.com/projets/couvertures-generatives
Charte graphique créée par Coraline Mas-Prévost, programme de génération créé par Louis Eveillard.

Un projet pour redonner à des livres numériques les couvertures qu’ils ont perdues pendant leur numérisation. Ces couvertures utilisent les méta-données issues de chaque livre. 

<a data-fancybox title="" href="https://louiseveillard.com/thumbs/projets/couvertures-generatives/couvertures_generatives-cover-1600x1167.jpg">![](https://louiseveillard.com/thumbs/projets/couvertures-generatives/couvertures_generatives-cover-1600x1167.jpg)</a>

## Julien Maire

+ Julien Maire
+ [https://www.youtube.com/watch?v=IbJ0Rg20_5E](https://www.youtube.com/watch?v=IbJ0Rg20_5E)

<a data-fancybox title="" href="http://www.makery.info/wp-content/uploads/2014/10/1-IMG_9599.jpg">![](http://www.makery.info/wp-content/uploads/2014/10/1-IMG_9599.jpg)</a>

Man at Work propose un jeu conceptuel autour du mythique « cinéma en relief », une expression qui fait référence aux procédés ayant cherché à reproduire les principes de la vision binoculaire naturelle de l’être humain dans l’art cinématographique. À partir du projecteur « 1.0 / Man at Work » qu’il a développé, Julien Maire crée une animation image par image en projetant, en boucle, un enchaînement d’impressions 3D stéréolitho­graphiques. Ce dispositif de cinéma mécanique, qui procure une profondeur et une texture particulière à l’image, donne vie à un personnage, sorte de Sisyphe des temps modernes, qui creuse inlassablement un trou, dans un perpétuel recommencement.

Production : Agence WBT/D (Bruxelles), Fédération Wallonie-Bruxelles – Cellule arts numériques (Bruxelles), iMal (Bruxelles)

Soutiens : Formlabs, Consulat général de France à Québec
Source: [https://mmrectoverso.org/portfolio/julien-maire-man-at-work/](https://mmrectoverso.org/portfolio/julien-maire-man-at-work/)

+ [https://www.youtube.com/watch?v=IbJ0Rg20_5E](https://www.youtube.com/watch?v=IbJ0Rg20_5E)

<a data-fancybox title="" href="http://v2.nl/events/julien-maire-man-at-work/leadImage_large">![](http://v2.nl/events/julien-maire-man-at-work/leadImage_large)</a>

For this unique variation on 3D-cinema, Julien Maire made his images using a 3D-printer. Like so many prototypes from early cinema, the piece consists of just a short loop. This stereolithographic projector offers a conceptual game around 3D-cinema, what the French used to call ‘relief cinema’ (as in ‘bas-relief’).

-----

« Le sujet de la disparition de la pellicule ne suscite aujourd’hui plus aucun débat. Cependant la matérialité des images se trouve sans cesse re-questionnée et ce, notamment, dans le cinéma d’animation. Celui-ci tente de renouveler inlassablement  ses modes de représentation tout comme les principes de sa propre mise en mouvement.  

Les modes de captation, d’enregistrement et les supports de diffusion des images numériques  deviennent de plus en plus raffinés. Depuis peu, les écrans deviennent flexibles, effectuant en quelque sorte la même révolution que la photographie qui passa du support métallique ou du verre à la cellulose. Ironie historique : un écran flexible rend en théorie possible la fabrication d’une pellicule qui pourrait contenir tous les films, un support dont les photogrammes seraient en perpétuel changement. 

Cette pellicule infinie ne constitue qu’un énoncé théorique du travail que j’aimerai déployer au Fresnoy : je questionnerai  la matérialité de l’image numérique et ses modes d’enregistrement notamment en insérant directement au sein des capteurs numériques des sculptures « subminiatures ».  L’incrustation d’un corps exogène dans l’espace réduit de captation des images pourrait être assimilée à la technique du rayogramme.  Une autre piste de travail est de graver définitivement des images sur les capteurs de cameras pour tenter d’implémenter de la permanence dans ce qui semble voué à un flux permanent du saisissement de la réalité ». J.M

source: https://www.lefresnoy.net/fr/ecole/artistes-professeurs-invites/julien-maire

## [Possible structures](http://superficie.ink/)

<a data-fancybox title="" href="/assets/structures.png">![](/assets/structures.png)</a>

## Aaron Koblin

#### Radiohead / House of cards

<iframe width="770" height="430" src="https://www.youtube.com/embed/8nTFjVm9sTQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

> [Lien vers le dépôt Git](https://github.com/dataarts/radiohead)

#### This exquisite forest

<iframe width="770" height="430" src="https://www.youtube.com/embed/nnhJ1841K-8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

[http://www.aaronkoblin.com/project/this-exquisite-forest/](http://www.aaronkoblin.com/project/this-exquisite-forest/)


## Marco Barotti

+ [https://www.marcobarotti.com/THE-WOODPECKERS-1](https://www.marcobarotti.com/THE-WOODPECKERS-1)


<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/278284618?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

<a data-fancybox title="" href="/assets/barotti.jpg">![](/assets/barotti.jpg)</a>

## Stéphane Noël

Un très beau travail de Stéphane Noël, générant un PDF à partir de films (et des fichiers de sous-titres associés).

+ [http://movieprint.codedrops.net/](http://movieprint.codedrops.net/)

<a data-fancybox title="" href="/assets/movieprint.png">![](/assets/movieprint.png)</a>

<a data-fancybox title="" href="/assets/movieprint-2.png">![](/assets/movieprint-2.png)</a>

## Recode Project

Le Recode Project est un projet visant à "restaurer" ou "recoder" certaines des premières oeuvres des pionniers de l'art algorithmique, et ce à l'aide de Processing.

+ [http://recodeproject.com/](http://recodeproject.com/)

<a data-fancybox title="" href="/assets/recode-project0.png">![](/assets/recode-project0.png)</a>
<a data-fancybox title="" href="/assets/recode-project.png">![](/assets/recode-project.png)</a>


## [Le Tricodeur](https://louiseveillard.com/projets/le-tricodeur)

<a data-fancybox title="" href="https://louiseveillard.com/thumbs/projets/le-tricodeur/tricodeur-residence-large-1-1600x1067.jpg">![](https://louiseveillard.com/thumbs/projets/le-tricodeur/tricodeur-residence-large-1-1600x1067.jpg)</a>

<a data-fancybox title="" href="/assets/tricodeur2.png">![](/assets/tricodeur2.png)</a>

**Louis Eveillard** / Le Tricodeur explore la mise en relation de deux pratiques créatives liées entre elles par un héritage commun : le tricot et la programmation. 

## Claire Williams

[Site](http://www.xxx-clairewilliams-xxx.com)

<a data-fancybox title="" href="http://www.xxx-clairewilliams-xxx.com/site/assets/files/1046/moteur_zoom.png">![](http://www.xxx-clairewilliams-xxx.com/site/assets/files/1046/moteur_zoom.png)</a>

## Zdeněk Sýkora

<a data-fancybox title="Zdeněk Sýkora" href="/assets/sykora.png">![Marius Watz](/assets/sykora.png)</a>

[Zdeněk Sýkora](https://www.google.com/search?q=Zden%C4%9Bk+S%C3%BDkora&client=firefox-b-d&source=lnms&tbm=isch&sa=X&ved=0ahUKEwia0K-NwsrgAhXMxqQKHad6C7kQ_AUIDigB&biw=1391&bih=897)

## [Réseaux antagonistes génératifs](https://fr.wikipedia.org/wiki/R%C3%A9seaux_antagonistes_g%C3%A9n%C3%A9ratifs)

<a data-fancybox title="" href="/assets/ai1.png">![ai](/assets/ai1.png)</a>

+ [Robbie Barrat](https://www.artnome.com/news/2019/1/22/ai-artist-robbie-barrat-and-painter-ronan-barrot-collaborate-on-infinite-skulls)
+ [Art42](https://art42.net/)
+ [Mario Klingemann](http://quasimondo.com/)

La métaphore de Sol LeWitt s'applique de multiples façons dans l'art GAN. L'ensemble de données est comme la carte de règles, avec des règles créées par curation - et le réseau les interprète pour faire de l'art. Mais en plus, le réseau lui-même est aussi comme la carte de règles, et les générations individuelles ne sont que des interprétations / exécutions différentes de ces règles. Cela est conforme à l'idée que les œuvres individuelles ne sont que des «jetons» de quelque chose de plus grand - ce sont des ombres du réseau, l'œuvre d'art réelle.

Dans le même temps, si le réseau lui-même est une œuvre d'art, il est très étrange, car il ne peut pas être vu ou compris entièrement (contrairement à l'ensemble des règles responsables des œuvres génératives traditionnelles). Nous ne pouvons en obtenir que de petits aperçus à la fois. Je ne connais aucun autre type d'art où cela est vrai.

Robbie Barrat

## Ai WeiWei et Olafur Eliasson : [Moon](http://www.moonmoonmoonmoon.com/)

Launched on 9 November 2013 by Olafur Eliasson and Ai Weiwei, Moon offered people around the world an opportunity to connect with others online via a collaborative drawing platform that transcended international borders to celebrate creative expression and interaction. Eliasson and Ai decided to end the project in September 2017. The moon itself, and the patchwork of drawings created during its four years of existence, can still be explored here.

"Comment Internet ouvre-t-il une ère nouvelle d’expression créative au-delà des frontières géographiques, sociales, économiques et politiques ? Tel est l’intitulé de l'intervention d'Olafur Eliasson et Ai WeiWei, intervention qui prend comme point de départ la certitude que la liberté d’expression, comme « le vent et l’air », ne peut pas être arrêtée. Les deux artistes ne dissertent pas : ils inaugurent Moon, un projet porté en commun. Il s’agit d’une « plateforme collaborative » qui prend la forme d’une planète divisée en plusieurs milliers de parcelles carrées et vierges, sur lesquelles chacun est invité à faire une « marque », sa marque, écrite et/ou dessinée." 

Source: [Arts Hebdo Medias](http://artshebdomedias.com/article/170114-ai-weiwei-et-olafur-eliasson-la-liberte-expression-en-orbite/)

<a data-fancybox title="Ai WeiWei et Olafur Eliasson" href="/assets/moonwei.png">![Marius Watz](/assets/moonwei.png)</a>

## Patience

Patience is a clock which uses a human face to represent the passage of time. The eyes work in the same way as an analog clock’s hands, with the right eye indicating hours and the left eye indicating minutes. The mouth opens and closes to represent seconds.

+ [https://vimeo.com/178717133](https://vimeo.com/178717133)

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/178717133?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## [Marius Watz](http://mariuswatz.com/)

Object #5 is a parametric form developed with reference to previous 3D printed objects. 
The model was created in Processing and the parts CNC milled.

<a data-fancybox title="Marius Watz" href="/assets/watz.jpg">![Marius Watz](/assets/watz.jpg)</a>

## [Olivier van Herpt](http://oliviervanherpt.com/)

<a data-fancybox title="" href="http://talent.stimuleringsfonds.nl/2016/site/assets/files/1119/olivier3.jpg">![](http://talent.stimuleringsfonds.nl/2016/site/assets/files/1119/olivier3.jpg)</a>

[Solid vibration](http://oliviervanherpt.com/solid-vibrations/)


https://zkm.de/en/exhibition/2017/10/open-codes

## Kyle McDonald

+ Exhausting a crowd: https://github.com/kylemcdonald/ExhaustingACrowd

<a data-fancybox title="Kyle" href="/assets/kyle1.png">![Kyle](/assets/kyle1.png)</a>

## Golan Levin

+ Augmented Hand Series: [https://vimeo.com/111951283](https://vimeo.com/111951283)

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/111951283?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


## Pablo Garcia

+ [https://www.pablogarcia.org/profilograph-after-muybridge/](https://www.pablogarcia.org/profilograph-after-muybridge/)

<a data-fancybox title="" href="/assets/garcia.jpeg">![](/assets/garcia.jpeg)</a>

## Iris Van Herpen

+ [https://www.irisvanherpen.com/](https://www.irisvanherpen.com/)

<a data-fancybox title="" href="/assets/vanherpen1.png">![](/assets/vanherpen1.png)</a>

<a data-fancybox title="" href="/assets/vanherpen2.png">![](/assets/vanherpen2.png)</a>

## Madison Maxey

"Afin de pouvoir participer à la mode de demain, je pense qu'il est essentiel de donnaitre un petit peu le code.
Le futur des vêtement sera tout à fait responsive à nos corps."
On parle ici de vêtements qui pourraient chauffer quand il commence à faire froid, où s'illuminer en fonction de l'intensité lumineuse de l'environnement.

[https://www.madisonmaxey.com](https://www.madisonmaxey.com)

## Pauline van Dongen

"Concept de Liquid modernity introduit par le sociologue Zygmunt Bauman. Nos environnements sont en constante évolution et en changement constant et sont devenus liquides sous l'influence de la technologie. Pour moi, le futur de la mode est dynamic, adaptive et responsive."

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/105908842?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

------

+ [Iris Van Herpen](https://www.google.com/search?q=Iris+Van+Herpen&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiJrfnA-MngAhXG_KQKHeDvDT8Q_AUIDigB&biw=1553&bih=978#imgrc=n6R0sLdIMJhtxM:)
+ [Anneke Smelik](https://hetnieuweinstituut.nl/en/anneke-smelik-cybercouture)