---
title: Le monde physique
lang: fr-FR
---

<p class="lead">Cette page évoque certains éléments bien au-delà de ce workshop mais mettant en jeu exactement les mêmes logiques et principes.</p>

# Interface avec le monde physique

<a data-fancybox title="PiPhone" href="/assets/piphone.jpg">![PiPhone](/assets/piphone.jpg)</a>
> [PiPhone](http://www.davidhunt.ie/piphone-a-raspberry-pi-based-smartphone/)

Il est très intéressant d'utiliser Processing pour manipuler des fichiers numériques, créer des images, produire des sons, etc. Mais ce que nous voyons durant ce workshop pourra également nous servir pour interagir avec des éléments du monde *physique*.
Gérer des lumières sur une scène, déclencher certains événements d'une installation interactive, construire un robot, piloter un traceur, etc..

Les deux projets repris ci-dessous impliquent des enjeux cruciaux quant à l'accès au numérique, au partage des données et des connaissances, et à la puissance de la collaboration.

## Arduino

> https://www.arduino.cc/

L'idée ici sera de faire communiquer Arduino et Processing pour, par exemple, allumer une diode (ou démarrer une voiture..), quand je passe ma souris sur une image, ou que je clique sur un bouton ou, comme je l'avais vu un jour, faire sonner une cloche chaque fois qu'un nouvel article est posté sur Wikipedia. Pour bon nombre de projets, Processing ne sera même pas nécessaire. Vous migrerez alors vers d'autres langages, comme *Javascript* ou *Python*.

<a data-fancybox title="Arduino" href="/assets/arduino.png">![Arduino](/assets/arduino.png)</a>

L'interface d'Arduino est très proche de celle de processing. Le langage diffère légèrement, mais vous passerez sans souci de l'un à l'autre, après 2-3 heures de travail.

<a data-fancybox title="Arduino et Processing" href="http://fab.cba.mit.edu/classes/863.11/people/daniel.rosenberg/images/p9/p9_7.jpg">![Arduino et Processing](http://fab.cba.mit.edu/classes/863.11/people/daniel.rosenberg/images/p9/p9_7.jpg)</a>

## MakeyMakey

Le MakeyMakey est un dispositif d'émulation de clavier à partir d'objets du quotidien : la manipulation de tout objet conducteur relié au MakeyMakey va envoyer un signal à un ordinateur, qui réagira avec la fonction que vous avez défini, en fonction du logiciel que vous utilisez.

MakeyMakey propose un détournement "Do It Yourself" de la manette de jeu et du clavier : ce que vous voulez créer comme interaction ne dépend que de vous. Facile à utiliser, sans danger, il permet une infinité d'interaction avec un ordinateur. (src: http://labenbib.fr)

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/60307041?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## Raspberry Pi

> https://www.raspberrypi.org/

Raspberry Pi est un nano ordinateur. Il est, pour son prix (+-40 EUR), extrêmement puissant. Il s'agit ici de matériel(hardware) libre, comme l'est également la carte Arduino. Vous pourriez, si vous êtes motivés, construire cet ordinateur vous-même. Les plans sont en ligne.

Vous pouvez, avec un Raspberry Pi :

[Liste]

<a data-fancybox title="Raspberry Pi" href="/assets/raspberrypi.jpg">![Raspberry Pi](/assets/raspberrypi.jpg)</a>