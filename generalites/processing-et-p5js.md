---
title: Processing et P5js
lang: fr-FR
---

# Processing et P5js

## Processing

Processing est **à la fois un langage et une interface** pour éditer votre code. Il s'agit d'un logiciel libre qui doit être installé sur votre machine (il l'est sur toutes celles de l'école).
Descendant direct de **Design by numbers** de John Maeda (dont vous trouverez un très beau livre à la bibliothèque), Processing a vu le jour au MIT, en 2001.
Clairement **orienté création visuelle**, il permet d'aborder les concepts de base de la programmation (variables, boucles, conditions...) de façon relativement **simple et autonome**.


1. <a href="https://processing.org/download/" target="_blank">Téléchargez</a> et installez Processing
2. Ouvrez le programme
3. Editez votre code
4. Testez-le
5. Sauvez votre travail

```processing
void setup() {
    size(600,400);
}
void draw() {
    background(220);
    ellipse(mouseX,mouseY,30,30);
}
```

<a data-fancybox title="Processing" href="/assets/processing-cap.png">![Processing](/assets/processing-cap.png)</a>
<span class="legende">L'environnement de Processing.</span>

<div class="notification box-processing">
    <h4>Liens utiles</h4>
    <ul>
    <li><a href="https://processing.org/reference/" target="_blank">Documentation de Processing</a></li>
    <li><a href="http://learningprocessing.com/examples/chp03/example-03-02-mouseX-mouseY" target="_blank">Exemples</a></li>
    <li><a href="https://fr.flossmanuals.net/processing/introduction/" target="_blank">FLOSS manuals</a></li>
    </ul>
</div>

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;"><iframe src="https://player.vimeo.com/video/72611093?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

> DBN (Design by numbers, par John Maeda, MIT), à l'origine de Processing

## P5js

P5js est ce que l'on appelle une **bibliothèque** (library) javaScript. Les fonctions utilisées sont très proches, dans l'esprit et dans la syntaxe, de Processing. Cela signifie que vos sketchs peuvent s'exécuter **directement dans votre navigateur**.
Bien qu'une connaissance des langages HTML et CSS, ainsi que des notions de base liées au web, soit un plus, P5js propose un **éditeur de code en ligne** très pratique, qui vous permet de rentrer directement dans le vif du sujet sans connaissance préalable. Une fois un compte créé, il vous est alors possible de sauver vos sketchs en ligne et de les **partager** directement.
Ces fonctions de sauvegarde et de partage en font un outil particulièrement intéressant.

1. RDV sur [https://editor.p5js.org/](https://editor.p5js.org/)
2. Créez un compte en ligne
3. Editez votre code
4. Partagez!

```javascript
function setup() {
    createCanvas(600,400);
}
function draw() {
    background(200);
    ellipse(mouseX,mouseY,30,30);
}
```

<a data-fancybox title="P5js" href="/assets/p5js-cap.png">![P5js](/assets/p5js-cap.png)</a>
<span class="legende">Le "même" programme avec P5js. Notez les quelques petites différences de syntaxe.</span>

<div class="notification box-p5js">
    <h4>Liens utiles</h4>
    <ul>
    <li><a href="https://p5js.org/reference/" target="_blank">Documentation de P5js</a></li>
    <li><a href="https://p5js.org/examples/" target="_blank">Exemples</a></li>
    <li><a href="https://www.youtube.com/user/shiffman/playlists?view=50&sort=dd&shelf_id=14" target="_blank">P5js en vidéo, par Daniel Shiffman</a></li>
    </ul>
</div>

## Passer de Processing à P5js

Passer un sketch Processing vers P5js est assez simple. Seuls quelques changements sont nécessaire.

Le code ci-dessous montre quelques éléments que vous aurez à *traduire* pour passer de Processing à javaScript (P5js). Vous pouvez, en guise de bac à sable (et bien plus encore..), utiliser l'[éditeur en ligne de P5js](https://editor.p5js.org/).

1. Remplacer les mots-clés **void** par le mot **function** (pour setup et draw)
2. Remplacer la fonction **size(largeur, hauteur)** par **createCanvas(largeur, hauteur)**
3. **Plus besoin de typer les variables** (int, float, etc.) : Juste utiliser **var** pour déclarer une variable.

```javascript
var posX;
var diametre = 100;

function setup() {
    createCanvas(windowWidth,windowHeight);
    background(240);
    noStroke();
    posX = -(diametre/2);
}
function draw() {
    background(240);
    fill(255,0,0);
    ellipse(posX,mouseY,diametre,diametre);
    posX+=7;
    if(posX>width+(diametre/2)) {
        posX = -(diametre/2);
    }
}
```

La liste complète et détaillée est reprise ici:

+ [https://github.com/processing/p5.js/wiki/Processing-transition](https://www.media.mit.edu/)
+ [Cette page](http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html) reprend de façon également exhaustive les différences entre Processing et P5js ainsi que la façon de passer de l'un à l'autre.
+ [http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html](http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html)
+ [Convertisseur en ligne](http://faculty.purchase.edu/joseph.mckay/p5jsconverter.html)

------

+ http://gerard.paresys.free.fr/P/index.html
+ http://purin.co/Experiments-with-P5-js