---
title: Scratch & Micro:bit
lang: fr-FR
---

<p class="lead">Il n'est pas toujours nécessaire de travailler avec du code (texte) pour créer un jeu, produire du son, ou interagir avec un corps dans l'espace. Vous trouverez ci-dessous deux projets qui utilisent la programmation par blocs.</p>

Créés pour les jeunes publics, ce sont des outils qui vont permettront de faire énormément de choses et de mettre en oeuvre certains projets très facilement.

## Scratch

> [https://scratch.mit.edu/](https://scratch.mit.edu/)

Scratch est un langage de programmation graphique manipulable et exécutable par le logiciel de même nom à vocation éducative. Ainsi, Scratch est à la fois un environnement de développement et un moteur d’exécution du langage Scratch mais aussi un site web.

<a data-fancybox title="SCratch" href="/assets/scratch.png">![SCratch](/assets/scratch.png)</a>

## Micro:bit

> [https://microbit.org/fr/](https://microbit.org/fr/)

Le micro:bit (aussi noté BBC micro:bit ou micro bit) est un ordinateur à carte unique3 doté d'un processeur ARM. Conçu au Royaume-Uni pour un usage éducatif dans un premier temps, le nanoordinateur est maintenant disponible au grand public dans de nombreux pays.

La platine de 4 × 5 cm embarque un processeur ARM Cortex-M0, un capteur de mouvement 3D (ou accéléromètre) et un magnétomètre 3D (ou boussole numérique), des connectiques Bluetooth et USB, une matrice de 5 x 5 DEL (25 diodes électroluminescentes), un bouton de réinitialisation et deux boutons programmables3. Le petit circuit imprimé peut être alimenté par un connecteur USB sur le port micro-USB ou par deux piles AAA (3V) en série sur un autre connecteur.

https://fr.wikipedia.org/wiki/Micro:bit

<a data-fancybox title="Microbit" href="/assets/microbit1.png">![Microbit](/assets/microbit1.png)</a>

<a data-fancybox title="Microbit" href="/assets/microbit2.png">![Microbit](/assets/microbit2.png)</a>

