---
title: Valeurs aléatoires
lang: fr-FR
---

# Valeurs aléatoires

Voici un exemple de sketch utilisant la fonction [**random()**](https://www.processing.org/reference/random_.html) de Processing, renvoyant un nombre aléatoire dans une fourchette donnée :

<iframe class="p5embed" width="740" height="403" src="https://editor.p5js.org/embed/S1xdhG98c7"></iframe>

```processing
float r;
float v;
float b;
float a;

float longueur;
float epaisseur;
float posX;
float posY;

void setup() {
    size(750,400);
}
void draw() {
    r = random(255);
    v = random(255);
    b = random(255);
    a = random(255);
    longueur = random(100);
    epaisseur = random(10);
    posX = random(-50,width);
    posY = random(height);

    strokeWeight(epaisseur);
    stroke(r,v,b,a);
    line(posX, posY, posX+longueur, posY);
}
```

## random()

Voyons différents cas de figure pour la fonction random() :

```processing
random(0,100); // Renverra un nombre entre 0 et 100
random(100); // Idem, version raccourcie
random(50,100); // Renverra un nombre entre 50 et 100
random(-50,150); // Renverra un nombre entre -50 et 150
random(width); // Renverra un nombre entre 0 et la largeur de votre sketch
```
Notez que la fonction **random() renverra toujours un nombre à virgule flottante (ex: 2.5358967)**

Pour arrondir, si nécessaire, à un nombre entier (integer), nous utiliserons les fonction **floor()** (arrondir à l'entier inférieur), **ceil()** (arrondir à l'entier supérieur), ou **round()** (arrondir à l'entier le plus proche). Par exemple :

```processing
floor(random(50)); // Renverra un entier entre 0 et 50
```


```processing
float nombre = 325;

void setup() {
    size(750, 400);
    noFill();
    smooth();
    //frameRate(2);
}

void draw() {
    stroke(0,140);
    background(230);
    for (var i=0; i < height; i+=5) {
        bezier(0, i, random(nombre), random(nombre), random(nombre), random(nombre), width, i);
    }
}
```

<iframe class="p5embed"  width="740" height="420" src="https://editor.p5js.org/embed/H1jjpSD5Q"></iframe>

## noise()

La fonction **noise()**, plutôt que de renvoyer des valeurs totalement aléatoires, renverra chaque fois des valeurs liées aux précédentes. De ce fait, les transitions sembleront continues. On parle ici de [bruit de Perlin](https://www.google.be/search?q=bruit+de+perlin&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjV94PTt_TdAhXMsKQKHYIpDMoQ_AUIDigB&biw=1277&bih=539) (2 ou 3 dimensions), du nom du fondateur du Media Research Lab de NYU, [Ken Perlin](https://fr.wikipedia.org/wiki/Ken_Perlin). On utilise cette technique notamment pour modéliser des textures de reliefs, de textiles ou de nuages par exemple.

<a data-fancybox title="Noise" href="/assets/noise.png">![Noise](/assets/noise.png)</a>
> [https://www.youtube.com/watch?v=qxJryrxSIQI](https://www.youtube.com/watch?v=qxJryrxSIQI)

----

### En savoir plus :

+ [https://www.youtube.com/watch?v=IKB1hWWedMk](https://www.youtube.com/watch?v=IKB1hWWedMk)

-----

Notez qu'il n'y a pas de véritable hasard sur un ordinateur. Voici ce qu'en disent Jean-Noël Lafargue et Jean-Michel Géridan, dans leur livre *"Processing, Le code comme outil de création"* :

> Tous les ordinateurs modernes embarquent une horloge interne qui permet au système de déterminer le nombre de millisecondes qui s'est écoulé depuis une date donnée (typiquement, le 1er janvier 1970). En interrogeant ce chiffre pendant l'exécution d'un programme et en le soumettant à un algorithme dédié, on peut obtenir un chiffre pseudo-aléatoire. Il ne s'agit pas réellement de hasard puisqu'en théorie, si on exécutait deux programmes en même temps sur deux ordinateurs dont l'horloge est réglée sur la même heure au limmième de seconde près, on obtiendrait le même résultat.
Ce type de hasard est tout de même suffisant dans de nombreux cas.