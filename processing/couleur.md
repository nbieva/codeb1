---
title: La couleur
lang: fr-FR
---

# La couleur

Par défaut, Processing utilise le modèle RVB, en codant les valeurs entre 0 et 255 (256 valeurs, voir plus bas)
Le changement de mode colorimétrique d'un sketch se fait à l'aide de la fonction colorMode(), de la façon suivante.

```processing
colorMode(RGB); // ou colorMode(HSB);
```
On peut également modifier l'échelle de valeurs (initialement 255) en entrant des paramètres supplémentaires.

```processing
colorMode(RGB, 100, 100, 100);
```

Plus d'infos sur [colorMode()](https://processing.org/reference/colorMode_.html)

<a data-fancybox title="" href="/assets/couleur-1.png">![](/assets/couleur-1.png)</a>

## Modèle RVB(A)

Le mode RVBA (rouge, vert, bleu, opacité) est peut-être le plus facile à mettre en oeuvre dans un premier temps.
Il s'utilise en inscrivant les valeurs désirées, séparées par des virgules, dans la fonction concernée. Par exemple:

```processing
fill(255,255,0,128); //Jaune à 50% d'opacité
```

Notez que fill() et stroke() ne s’appliquent qu’à ce qui suit, dans l’ordre du programme. 
Ainsi, un rectangle dessiné **AVANT** l’instruction ne sera pas affecté.

#### Pourquoi 256 valeurs?

Les valeurs s'expriment sur une échelle de 0 à 255 (256 valeurs)
256 car une couleur se code ici sur 1 octet. 1 octet c'est 8 bits. et 1 bit c'est juste un switch entre 1 ou 0.
Un octet pourrait être ceci: 11010001
Je peux, sur un octet, définir 256 (2 exposant 8) possibilités:

+ 01111111
+ 10111111
+ 11011111
+ 11101111
+ etc..

Il n'est pas forcément évident de supposer les valeurs à entrer pour une couleur donnée. Il est plus facile d'aller les chercher (copy/paste) dans un sélecteur de couleur (Celui d'Illustrator par exemple). Ou tout simplement en faisant une recherche google sur [**color selector**](https://www.google.be/search?q=color+selector&oq=color+selector&aqs=chrome..69i57.2509j0j7&sourceid=chrome&ie=UTF-8)

<a data-fancybox title="" href="/assets/couleur-2.png">![](/assets/couleur-2.png)</a>
<a data-fancybox title="" href="/assets/couleur-3.png">![](/assets/couleur-3.png)</a>

## Mode HSB

Le mode HSB, lui, s'exprime en radians (ou degrés). Un peu plus difficile à appréhender au départ, il permet des résultats souvent plus riches en nuances.

<div style="margin:20px 20%">

<a data-fancybox title="" href="https://processing.org/tutorials/color/imgs/hsb.png">![](https://processing.org/tutorials/color/imgs/hsb.png)</a>

</div>

> [Source](https://processing.org/tutorials/color/)

```processing
colorMode(HSB, 360, 100, 100);
```

[About circles](http://bluegalaxy.info/codewalk/2017/11/28/p5-js-understanding-circles/)

## Les modes de fusion

La fonction **blendMode()** permet de gérer les différents modes de fusion des couleurs dans Processing (ou P5js).

```processing
blendMode(ADD);
//or
blendMode(MULTIPLY);
//or
blendMode(DIFFERENCE);
//or
blendMode(EXCLUSION);
//or
blendMode(DARKEN);
// Etc.
```

<div style="margin:30px 20%;">

<a data-fancybox title="" href="/assets/difference.jpg">![](/assets/difference.jpg)</a>

</div>