---
title: Les transformations
lang: fr-FR
---

# Les transformations

Dans Processing, les transformations s'apppliquent à tout le système de coordonnées. De ce fait, elles ne sont pas faciles à appréhender. Vous trouverez de plus amples informations sur [cette page des Floss Manuals](https://fr.flossmanuals.net/processing/les-transformations/) (cette page vient de là).

## Translation (translate)

La fonction **translate()** modifie le point d'origine du système de coordonnées pour tout ce qui va être dessiné à sa suite.

L'avantage, par rapport à une utilisation de variables ici, est que le code qui concerne directement notre forme ne change pas, aussi complexe soit-elle. Seul le point d'origine des coordonnées change. Le forme peut alors être déplacée.

<a data-fancybox title="" href="/assets/translate.png">![](/assets/translate.png)</a>

Par exemple:

```processing
void setup() {
    size(800,400);
}
void draw() {
    background(240);
    fill(255,150,50);
    //Ma translation
    translate(mouseX, mouseY);
    // Ma forme
    beginShape();
        vertex(0,0);
        vertex(90,20);
        vertex(60,40);
        vertex(90,40);
        vertex(60,60);
        vertex(90,60);
        vertex(60,80);
        vertex(70,90);
        vertex(60,100);
        vertex(50,90);
        vertex(40,100);
        vertex(0,70);
        vertex(10,40);
    endShape(CLOSE);
}
```

## Rotation (rotate)

<iframe class="p5embed" width="740" height="310" src="https://editor.p5js.org/embed/B1xVhZWi7"></iframe>

Un exemple simple de rotation:

```processing
void setup() {
    size(750, 300);
    fill(249, 229, 112);
    stroke(242, 200, 48);
    strokeWeight(10);
    //Dessine les rectangles à partir de leur centre (et non le coin supérieur droit)
    rectMode(CENTER);
}

void draw() {
    // Si vous voulez donner l'impression qu'un élément tourne sur lui-même, il vous faudra à chaque boucle redessiner un background pour masquer les éléments précédents.
    background(188, 182, 196);
    pushMatrix();// Isole les transformations qui suivent, jusque popMatrix()
    // Translation : Déplace le point d'origine du système de coordonnées.
    translate(width/2, height/2); 
    // Rotation. Tout le système de coordonnées effectue une rotation.
    rotate(frameCount / 10.0); //On utilise la variable frameCount
    rect(0,0,200,100);
    popMatrix();//Rétabli l'état initial
}
```

<iframe class="p5embed" width="740" height="395" src="https://editor.p5js.org/embed/B030cC-r8"></iframe>

> [Plus d'infos sur les rotations dans Processing](http://btk.tillnagel.com/tutorials/rotation-translation-matrix.html)

## Cumul
Attention, gardez à l'esprit que les transformations s'accumulent dans Processing. Si vous redéfinissez un **translate(0,0)** après un **translate(50,50)**, une forme dessinée et placée au point **0,0** sera en fait positionnée au point **50,50** dans votre sketch. 

Vous trouverez plus d'informations sur les transformations sur [cette page des FLOSS Manuals FR](https://fr.flossmanuals.net/processing/les-transformations/) ou en faisant une recherche sur le web..

<div class="notification box-processing onelink" style="margin-top:3rem;">
<p>
    Les <a href="https://fr.flossmanuals.net/processing/les-transformations/" target="_blank">transformations</a> dans Processing
</p>
</div>
