---
title: Les variables
lang: fr-FR
sidebarDepth: 1
---

# Les variables

L'on peut définir une variable comme un espace réservé dans la mémoire de votre ordinateur pour y stocker une valeur (d'un certain type). Cette valeur va pouvoir par la suite être utilisée, manipulée, modifiée, remplacée.
La position en X et en Y de votre souris sont deux variables. Déplacez votre souris et les valeurs de ces variables seront modifiées.

Dans Processing, il nous faut définir un type pour chaque variable créée. Ce n'est pas nécessaire en javaScript par exemple.
**Les principaux types** sont les suivants:

+ **int** : nombre entier (.. ,-2 ,-1 ,0 ,1 ,2 ,.. )
+ **float** : nombre à virgule flottante (0.5263, 452.18, 1.0, ..)
+ **string** : chaîne de caractères, à mettre entre guillemets ( "Peinture", "Photographie", ..)
+ **boolean** : variable booléenne. Sera vraie ou fausse (**true** or **false**)
+ **color** : sert à stocker une couleur

Vous en trouverez d'autres [ci-dessous](#les-differents-types-de-variables).

Les différentes étapes dans l'utilisation d'une variable sont les suivantes:

+ On déclare la variable (ou les variables)
+ On lui assigne une valeur
+ On l'utilise

```processing
int monNombre;

void setup() {
    size(500,500);
}
void draw() {
    ellipse(mouseX, mouseY, 5, 5);
}
```
On peut aussi combiner les deux premières étapes comme suit:

```processing
int monNombre;

void setup() {
    size(500,500);
    monNombre = 100;
}
void draw() {
    ellipse(monNombre, monNombre, monNombre/2,  monNombre/2);
    // Dessinera un cercle de 50 sur 50 positionné en 100,100
}
```

Notez que:

+ On ne déclare le type qu'une seule fois dans le programme
+ Les noms de variables ne comprennent ni espaces, ni accents, ni chiffres, et commencent par une minuscule.
+ Certains noms de variables sont "réservés", par les variables natives notamment (mouseX, frameCount, ..)

<a data-fancybox title="" href="/assets/variables.jpg">![](/assets/variables.jpg)</a>

## Incrémenter une variable

```processing
//Déclarez les deux variables pour les positions en X et Y de votre éllipse
int posX = 0;
int posY = 150;

void setup() {
  size(750, 300);
  noStroke(); // Pas de contour
  ellipseMode(CENTER);
}

void draw() {
    background(220);
    fill(255,0,0); //rouge
    ellipse(posX, posY, 100, 100);

    // On incrémente la position en X de 1 à chaque boucle
    posX = posX + 1; // Peut aussi s'écrire posX += 1;
    if (posX == 800) {
        posX = -50;
    }
}
```

<iframe class="p5embed" width="740" height="320" src="https://editor.p5js.org/embed/B12v2K8qQ"></iframe>

## Variables natives

Il existe dans Processing une série de variables natives telles que **mouseX** et **mouseY**.
Si j'utilise mouseX et mouseY dans mon sketch (à l'intérieur de la boucle draw en l'occurrence..), ces variables seront remplacées par la position en X et en Y de ma souris. Ainsi pour créer un simple outil de dessin, nous pouvons utiliser le code qui suit:

```processing
void setup() {
    size(500,500);
}
void draw() {
    ellipse(mouseX, mouseY, 5, 5);
}
```

Il existe d'autres variables natives dans processing, dont les plus importantes sont:

+ **width** : largeur de votre sketch
+ **height** : hauteur de votre sketch
+ **frameCount** : index de la boucle (sorte de compteur)
+ **pmouseX** : position précédente de la souris en X
+ **pmouseY** : position précédente de la souris en Y

Ainsi, on peut affiner notre sketch précédent comme suit:

```processing
void setup() {
    size(750,400);
}
void draw() {
    line(pmouseX, pmouseY, mouseX, mouseY);
}
```

<iframe class="p5embed" width="740" height="410" src="https://editor.p5js.org/embed/SJaUSGCYm"></iframe>

On peut aussi lier une valeur (ici le diamètre du disque) à une autre variable (ici la position de la souris en Y) :

<iframe class="p5embed" width="740" height="310" src="https://editor.p5js.org/embed/HJi0tzAKX"></iframe>


Créez un sketch dans lequel varient un grand nombre de variables simultanément (background, hauteurs, largeurs, contours, couleurs, etc.)


## Les différents types de variables
>Source: [Floss manuals](https://fr.flossmanuals.net/processing/les-variables/)

Une variable est une donnée que l'ordinateur va stocker dans l'espace de sa mémoire. C'est comme un compartiment dont la taille n'est adéquate que pour un seul type d'information. Elle est caractérisée par un nom qui nous permettra d'y accéder facilement.

<a data-fancybox title="illustration_variables_memoire_1.png" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-illustration_variables_memoire_1-fr-old.png">![illustration_variables_memoire_1.png](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-illustration_variables_memoire_1-fr-old.png)</a>

Il existe différents type de variables : des nombres entiers (int), des nombres à virgule (float), du texte (String), des valeurs vrai/faux (boolean). Un nombre à décimales, comme 3,14159, n'étant pas un nombre entier, serait donc du type float. Notez que l'on utilise un point et non une virgule pour les nombres à décimales. On écrit donc 3.13159. Dans ce cas, les variables peuvent être annoncées de cette manière :
```processing
float x = 3.14159;
int y = 3;
```
Le nom d'une variable peut contenir des lettres, des chiffres et certains caractères comme la barre de soulignement. À chaque fois que le programme rencontre le nom de cette variable, il peut lire ou écrire dans ce compartiment. Les variables qui vont suivre vous donneront des exemples simples de leur utilisation. Pour résumer, une variable aura un type, un nom et une valeur qui peut être lue ou modifiée.

## int
Dans la syntaxe de Processing, on peut stocker un nombre entier, par exemple 3, dans une variable de type int.
```processing
int entier;
entier = 3;
print(entier);
```
<a data-fancybox title="var_int.tiff" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-var_int-tiff-fr-old.png">![var_int.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-var_int-tiff-fr-old.png)</a>

## float

Il s'agit d'un nombre avec décimales, par exemple 2,3456.
```processing
float decimal;
decimal = PI;
print(decimal);
```
<a data-fancybox title="float_var.tiff" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-float_var-tiff-fr-old.png">![float_var.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-float_var-tiff-fr-old.png)</a>
## double

Il s'agit également de nombre avec décimales, mais qui fournissent davantage de précision que le type float.
```processing
double long_decimal;
long_decimal = PI;
print(long_decimal);
```
<a data-fancybox title="double_var.tiff" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-double_var-tiff-fr-old.png">![double_var.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-double_var-tiff-fr-old.png)</a>

## boolean

Il s'agit d'un type de variable qui ne connaît que deux états : Vrai (true) ou Faux (false). Elle est utilisée dans les conditions pour déterminer si une expression est vraie ou fausse.
```processing
boolean vraifaux;
vraifaux = true;
println(vraifaux);
```
<a data-fancybox title="char_bool" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-char_bool-fr-old.jpg">![char_bool](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-char_bool-fr-old.jpg)</a>

## char

Cette variable sert à stocker un caractère typographique (une lettre). Notez l'usage de ce qu'on appelle des guillemets simples.
```processing
char lettre;
lettre = 'A';
print(lettre);
```
<a data-fancybox title="char_var.tiff" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-char_var-tiff-fr-old.png">![char_var.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-char_var-tiff-fr-old.png)</a>

## string

Cette variable sert à stocker du texte. Notez l'usage des guillemets doubles.
```processing
String texte;
texte = "Bonjour!";
print(texte);
```
<a data-fancybox title="string_var.tiff" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-string_var-tiff-fr-old.png">![string_var.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-string_var-tiff-fr-old.png)</a>

## color

Sert à stocker une couleur. Cette variable est utile lorsqu'on veut réutiliser souvent les mêmes couleurs.

<a data-fancybox title="damier.tiff" href="https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-damier-tiff-fr-old.png">![damier.tiff](https://fr.flossmanuals.net/processing/les-variables/static/Processing-Variables-damier-tiff-fr-old.png)</a>
```processing
noStroke();
color blanc = color(255, 255, 255);
color noir = color(0, 0, 0);

fill(blanc); rect(0, 0, 25, 25);
fill(noir); rect(25, 0, 25, 25);
fill(blanc); rect(50, 0, 25, 25);
fill(noir); rect(75, 0, 25, 25);

fill(noir); rect(0, 25, 25, 25);
fill(blanc); rect(25, 25, 25, 25);
fill(noir); rect(50, 25, 25, 25);
fill(blanc); rect(75, 25, 25, 25);

fill(blanc); rect(0, 50, 25, 25);
fill(noir); rect(25, 50, 50, 25);
fill(blanc); rect(50, 50, 75, 25);
fill(noir); rect(75, 50, 100, 25);

fill(noir); rect(0, 75, 25, 25);
fill(blanc); rect(25, 75, 25, 25);
fill(noir); rect(50, 75, 25, 25);
fill(blanc); rect(75, 75, 25, 25);
```

### En savoir plus :

+ [https://fr.flossmanuals.net/processing/les-variables/](https://fr.flossmanuals.net/processing/les-variables/)